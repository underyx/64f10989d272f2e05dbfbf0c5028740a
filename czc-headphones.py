#!/usr/bin/env python3
import os
from requests_html import HTMLSession

session = HTMLSession()

webhook_url = os.environ["SLACK_WEBHOOK_URL"]

r = session.get("https://www.czc.cz/sony%20wh-1000xm2/hledat")
r.raise_for_status()

tiles = r.html.find(".new-tile")
print(tiles)

price = min(
    int(
        tile.find(".price .price-vatin", first=True)
        .text.replace("\xa0", "")
        .replace("Kč", "")
    )
    for tile in tiles
)
print(price)

if price != 7790:
    r = session.post(
        webhook_url,
        json={
            "text": "@bence buy the headphones, their price changed! https://www.czc.cz/sony%20wh-1000xm2/hledat",
            "link_names": True,
        },
    )
    r.raise_for_status()
    print(r.text)
